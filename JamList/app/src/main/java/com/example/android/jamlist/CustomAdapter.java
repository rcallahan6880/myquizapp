package com.example.android.jamlist;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class CustomAdapter extends BaseAdapter {

    Context context;
    List<RowItem> rowItems;

    CustomAdapter(Context context, List<RowItem> rowItems) {


        this.context = context;
        this.rowItems = rowItems;
    }

    @Override
    public int getCount() {
        return rowItems.size();
    }

    @Override
    public Object getItem(int position) {
        return rowItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return rowItems.indexOf(getItem(position));
    }

    private class ViewHolder {
        ImageView album_cover;
        TextView artist;
        TextView genres;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;

        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.list_item, parent,false);
            holder = new ViewHolder();

            holder.artist = convertView.findViewById(R.id.artist);
            holder.album_cover =  convertView.findViewById(R.id.album_cover);

            RowItem row_pos = rowItems.get(position);

            holder.album_cover.setImageResource(row_pos.getAlbum_cover_id());
            holder.artist.setText(row_pos.getArtist());
            holder.genres.setText(row_pos.getGenres());
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        return convertView;
    }
}





