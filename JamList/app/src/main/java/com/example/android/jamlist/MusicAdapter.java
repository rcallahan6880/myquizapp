//package com.example.android.jamlist;
//
//import android.app.Activity;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.ArrayAdapter;
//import android.widget.TextView;
//
//import java.util.ArrayList;
//
//
//public class MusicAdapter extends ArrayAdapter<Songs> {
//        public MusicAdapter(Activity context, ArrayList<Songs> songs){
//            super(context,0, songs);
//        }
//
//        @Override
//        public View getView(int position, View convertView, ViewGroup parent) {
//            //Word currentWord = getItem(position);
//            View listItemView = convertView;
//            if (listItemView == null){
//                listItemView = LayoutInflater.from(getContext()).inflate(R.layout.list_songs,parent, false);
//            }
//
//            // Get the {@link AndroidFlavor} object located at this position in the list
//            Songs currentSong = getItem(position);
//
//            // Find the TextView in the list_item.xml layout with the ID version_name
//            TextView songsTextView = listItemView.findViewById(R.id.album_cover);
//            // Get the version name from the current AndroidFlavor object and
//            // set this text on the name TextView
//            songsTextView.setText(currentSong.getMiwokTranslation());
//
//            // Find the TextView in the list_item.xml layout with the ID version_number
//            TextView defaultTextView = listItemView.findViewById(R.id.default_text_view);
//            // Get the version number from the current AndroidFlavor object and
//            // set this text on the number TextView
//            defaultTextView.setText(currentSong.getDefaultTranslation());
//
//            // Find the ImageView in the list_item.xml layout with the ID list_item_icon
////    ImageView iconView = (ImageView) listItemView.findViewById(R.id.list_item_icon);
//            // Get the image resource ID from the current AndroidFlavor object and
//            // set the image to iconView
////        iconView.setImageResource(currentAndroidFlavor.getImageResourceId());
//
//            // Return the whole list item layout (containing 2 TextViews and an ImageView)
//            // so that it can be shown in the ListView
//            return listItemView;
//        }
//
//}
