package com.example.android.jamlist;

public class RowItem {
    private String genres;
    private String artist;
    public int album_cover_id;

    public RowItem(String genres, int album_cover_id, String artist) {

        this.genres = genres;
        this.album_cover_id = album_cover_id;
        this.artist = artist;
//        this.contactType = contactType;
    }

    public String getGenres() {
        return genres;
    }

    public void setGenres(String genres) {
        this.genres = genres;
    }

    public int getAlbum_cover_id() {
        return album_cover_id;
    }

    public void setAlbum_cover_id(int profile_pic_id) {
        this.album_cover_id = album_cover_id;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

//    public String getContactType() {
//        return contactType;
//    }
//
//    public void setContactType(String contactType) {
//        this.contactType = contactType;
//    }
}
