package com.example.android.jamlist;

import android.os.Bundle;
import android.widget.ListView;

public class Songs {
    private String mDefaultTranslation;

    private String mMiwokTranslation;

    public Songs(String defaultTranslation, String miwokTranslation){
        mDefaultTranslation = defaultTranslation;
        mMiwokTranslation = miwokTranslation;
    }

    /**
     * get translation
     */
    public String getDefaultTranslation(){
        return  mDefaultTranslation;
    }

    /**
     * get miwok translation
     */
    public String getMiwokTranslation(){
        return mMiwokTranslation;
    }
}
