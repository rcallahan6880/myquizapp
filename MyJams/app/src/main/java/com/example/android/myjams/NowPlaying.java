//package com.example.android.myjams;
//
//import android.content.Intent;
//import android.os.Bundle;
//import android.support.v7.app.AppCompatActivity;
//import android.view.View;
//import android.widget.AdapterView;
//import android.widget.ArrayAdapter;
//import android.widget.ListView;
//
//import static android.provider.AlarmClock.EXTRA_MESSAGE;
//
//public class NowPlaying extends AppCompatActivity{
//    ListView listView;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.now_playing);
//        listView = findViewById(R.id.list);
//        String[] values = new String[] {"button1","button2","button3"};
//        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
//                android.R.layout.activity_list_item, android.R.id.text1,values);
//        listView.setAdapter(adapter);
//        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view,
//                                    int position, long id) {
//                if (position == 0){
//                    Intent myIntent = new Intent(view.getContext(),Music.class);
//                    startActivity(myIntent,0);
//                }
//                if (position == 1){
//                    Intent myIntent = new Intent(view.getContext(),Music.class);
//                    startActivity(myIntent,1);
//                }
//
//            }
//        });
//    }
//
//}
