package com.example.android.myjams;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//     find the view for numbers
        TextView hickhop = findViewById(R.id.hickhop);

// Set a click listener on that View
        hickhop.setOnClickListener(new View.OnClickListener() {
            // The code in this method will be executed when the numbers View is clicked on.
            @Override
            public void onClick(View view) {
//  create intent for number activity
                Intent numbersIntent = new Intent(MainActivity.this, HickHop.class);
                startActivity(numbersIntent);
            }

        });

        TextView country = (TextView) findViewById(R.id.country);

// Set a click listener on that View
        country.setOnClickListener(new View.OnClickListener() {
            // The code in this method will be executed when the numbers View is clicked on.
            @Override
            public void onClick(View view) {
//  create intent for number activity
                Intent numbersIntent = new Intent(MainActivity.this, Country.class);
                startActivity(numbersIntent);
            }

        });
    }


}

