package com.example.android.myquizapp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import static com.example.android.myquizapp.MainActivity.score;

import java.util.ArrayList;


/**
 * Created by CallahanRa01 on 3/21/2018.
 */

public class second extends AppCompatActivity {

    public ArrayList<String> checkedBoxes = new ArrayList();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.second_page);
    }

    //        Submit Button to calculate scores and return message
    public void submitButton(View v) {
//        get q4 user answer
        EditText text1 = findViewById(R.id.q4AnswerInput);
        String answerQ4 = text1.getText().toString();
//        check q4 answer
        if (answerQ4.contains("2003")) {
            MainActivity.score += 1;
        }
//        get q5 user answer
        EditText text2 = findViewById(R.id.q5AnswerInput);
        String answerQ5 = text2.getText().toString();
//        check q5 user answer
        if (answerQ5.equalsIgnoreCase(getString(R.string.q5CorrectAnswerCheck))) {
            MainActivity.score += 1;
        }
//        check for the correct check boxes
        ArrayList<String> correctAnswer = new ArrayList<>();
        {
            correctAnswer.add(getString(R.string.staticArrayItem1));
            correctAnswer.add(getString(R.string.staticArrayItem2));
            correctAnswer.add(getString(R.string.staticArrayItem3));
            correctAnswer.add(getString(R.string.staticArrayItem4));
        }
        ArrayList<String> checkBoxList = checkedBoxes;

        if (checkBoxList.equals(correctAnswer)) {
            MainActivity.score += 1;
        }
//        set submit button to false so it cannot be clicked twice
        Button submitAnswers = findViewById(R.id.submitAnswers);
        submitAnswers.setClickable(false);

//        Check boolean value of radio buttons
        if (MainActivity.isCheckedq1answer3) {
            MainActivity.score += 1;
        }
        if (MainActivity.isCheckedq2answer1) {
            MainActivity.score += 1;
        }
        if (MainActivity.isCheckedq3answer4) {
            MainActivity.score += 1;
        }
        if (score == 6) {
            for (int i = 0; i < 3; i++) {
                Toast.makeText(this, R.string.sixAnswersCorrect, Toast.LENGTH_LONG).show();
            }
        } else if (score == 5) {
            for (int i = 0; i < 2; i++) {
                Toast.makeText(this, R.string.fiveAnswersCorrect, Toast.LENGTH_LONG).show();
            }
        } else if (score == 4) {
            for (int i = 0; i < 2; i++) {
                Toast.makeText(this, R.string.fourAnswersCorrect, Toast.LENGTH_LONG).show();
            }
        } else if (score == 3) {
            for (int i = 0; i < 2; i++) {
                Toast.makeText(this, R.string.threeAnswersCorrect, Toast.LENGTH_LONG).show();
            }
        } else if (score == 2) {
            for (int i = 0; i < 2; i++) {
                Toast.makeText(this, R.string.twoAnswersCorrect, Toast.LENGTH_LONG).show();
            }
        } else if (score == 1) {
            for (int i = 0; i < 2; i++) {
                Toast.makeText(this, R.string.oneAnswerCorrect, Toast.LENGTH_LONG).show();
            }
        } else if (score == 0) {
            for (int i = 0; i < 2; i++) {
                Toast.makeText(this, R.string.noAnswersCorrect, Toast.LENGTH_LONG).show();
            }
        }
//       change submit button text when clicked
        Button submitButton = findViewById(R.id.submitAnswers);
        submitButton.setText(R.string.clickedSubmitButton);
    }

    //      get number correct from checkboxes
    public void onCheckboxClicked(View view) {
//      Is the view now checked?
        boolean checked = ((CheckBox) view).isChecked();

//      Check which checkbox was clicked
        switch (view.getId()) {
            case R.id.q6answer1:
                if (checked) {
                    checkedBoxes.add(getString(R.string.checkBoxAnswer1));
                    return;
                } else
                    break;
            case R.id.q6answer2:
                if (checked) {
                    checkedBoxes.add(getString(R.string.checkBoxAnswer2));
                    return;
                } else
                    break;
            case R.id.q6answer3:
                if (checked) {
                    checkedBoxes.add(getString(R.string.checkBoxAnswer3));
                    return;
                } else
                    break;
            case R.id.q6answer4:
                if (checked) {
                    checkedBoxes.add(getString(R.string.checkBoxAnswer4));
                    return;
                } else
                    break;
            case R.id.q6answer5:
                if (checked) {
                    checkedBoxes.add(getString(R.string.checkBoxAnswer5));
                    return;
                } else
                    break;
            case R.id.q6answer6:
                if (checked) {
                    checkedBoxes.add(getString(R.string.checkBoxAnswer6));
                    return;
                } else
                    break;
        }

    }

    //        Reset the quiz
    public void resetQuiz(View v) {
//        reset checkboxes
        CheckBox q6answer1 = findViewById(R.id.q6answer1);
        q6answer1.setChecked(false);
        CheckBox q6answer2 = findViewById(R.id.q6answer2);
        q6answer2.setChecked(false);
        CheckBox q6answer3 = findViewById(R.id.q6answer3);
        q6answer3.setChecked(false);
        CheckBox q6answer4 = findViewById(R.id.q6answer4);
        q6answer4.setChecked(false);
        CheckBox q6answer5 = findViewById(R.id.q6answer5);
        q6answer5.setChecked(false);
        CheckBox q6answer6 = findViewById(R.id.q6answer6);
        q6answer6.setChecked(false);
//        reset text entries
        EditText text1 = findViewById(R.id.q4AnswerInput);
        text1.setText("");
        EditText text2 = findViewById(R.id.q5AnswerInput);
        text2.setText("");
//        set submitbutton back to clickable
        Button submitAnswers = findViewById(R.id.submitAnswers);
        submitAnswers.setClickable(true);
//        switch back to first page to reset radio buttons and reset boolean to false
        setContentView(R.layout.activity_main);
        RadioGroup qOne = findViewById(R.id.q1group);
        qOne.clearCheck();
        MainActivity.isCheckedq1answer3 = false;
        RadioGroup qTwo = findViewById(R.id.q2group);
        qTwo.clearCheck();
        MainActivity.isCheckedq2answer1 = false;
        RadioGroup qThree = findViewById(R.id.q3group);
        qThree.clearCheck();
        MainActivity.isCheckedq3answer4 = false;
        Intent reset = new Intent(getBaseContext(), MainActivity.class);
        startActivity(reset);
//        reset score
        score = 0;
    }

    public boolean dispatchTouchEvent(MotionEvent event) {
        View view = getCurrentFocus();
        boolean ret = super.dispatchTouchEvent(event);

        if (view instanceof EditText) {
            View w = getCurrentFocus();
            int scrcoords[] = new int[2];
            w.getLocationOnScreen(scrcoords);
            float x = event.getRawX() + w.getLeft() - scrcoords[0];
            float y = event.getRawY() + w.getTop() - scrcoords[1];

            if (event.getAction() == MotionEvent.ACTION_UP
                    && (x < w.getLeft() || x >= w.getRight()
                    || y < w.getTop() || y > w.getBottom())) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getWindow().getCurrentFocus().getWindowToken(), 0);
            }
        }
        return ret;

    }
}
