package com.example.android.myquizapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {
    static boolean isCheckedq1answer3 = false;
    static boolean isCheckedq2answer1 = false;
    static boolean isCheckedq3answer4 = false;
    static int score = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
//        make toast message stay longer
        for (int i = 0; i < 1; i++) {
            Toast.makeText(this, R.string.openeningMessage, Toast.LENGTH_LONG).show();
        }
    }

    public Button nextPage;

    public void init() {
        nextPage = findViewById(R.id.next_page);
        nextPage.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent screen = new Intent(MainActivity.this, second.class);
                startActivity(screen);
            }
        });
    }

    //   get number correct from radio buttons
    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();
        // Check which radio button was clicked
        switch (view.getId()) {
            case R.id.q1answer3:
                if (checked) {
                    isCheckedq1answer3 = true;
                    return;
                } else
                    break;
            case R.id.q2answer1:
                if (checked) {
                    isCheckedq2answer1 = true;
                    return;
                } else
                    break;
            case R.id.q3answer4:
                if (checked) {
                    isCheckedq3answer4 = true;
                    return;
                } else
                    break;

        }

    }

}



